name := "TrxProducer"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= {
  val sparkVer = "2.1.1"
  Seq(
    "org.apache.hadoop" % "hadoop-common" % "2.7.3",
    "org.apache.spark" %% "spark-core" % sparkVer % "provided" withSources(),
    "org.apache.spark" %% "spark-sql" % sparkVer,
    "org.scalanlp" %% "breeze" % "0.13.2",
    "org.apache.kafka" % "kafka-clients" % "0.10.1.0"
  )
}

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}