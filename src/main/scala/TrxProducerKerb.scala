import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.spark.sql.SparkSession

/**
  * Created by Ivan Dundovic on 16.3.2018..
  */
object TrxProducerKerb {
  def main(args: Array[String]): Unit = {
    val hours: Int = args(0).toInt
    val minutes: Int = args(1).toInt
    val seconds: Int = args(2).toInt
    val numRows: Int = args(3).toInt
    val path: String = args(4)
    val topic: String = args(5)

    val spark = SparkSession.builder()
      .master("yarn")
      .appName("TrxProducer")
      .getOrCreate()

    val props = new Properties()
    props.put("bootstrap.servers", "analytics-dn2:6667,analytics-dn3:6667,analytics-dn4:6667")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("security.protocol", "SASL_PLAINTEXT")

    val producer = new KafkaProducer[String, String](props)

    //"/user/PoC/Mercury/Mercury_Analytics/DataSet_secured_ClientTrxAnalyticsDaily_20160401_20160731/RAW/merchant_location_analytics_full_clean.parquet"
    val dfAllTrx = spark.read.parquet(path).take(numRows)


    var milliseconds: Int = 0
    if (hours > 0) {
      milliseconds += hours * 60 * 60 * 1000
    }
    if (minutes > 0) {
      milliseconds += minutes * 60 * 1000
    }
    if (seconds > 0) {
      milliseconds += seconds * 1000
    }

    val timeSleep = milliseconds / numRows

    for (i <- 0 to numRows) {
      Thread.sleep(timeSleep.toLong)
      val record = new ProducerRecord[String, String](topic, i % 12, i.toString(), dfAllTrx(i).toString())
      //println(record)
      producer.send(record)
    }

    producer.close()

    spark.stop()
  }
}
